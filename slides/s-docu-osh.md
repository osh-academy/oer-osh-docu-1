---
type: slide
slideOptions:
  transition: slide
---

<img src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo.svg" style="border: none;background: none;box-shadow:none" height="600">

---

# Documentation

**of**

## Open Source Hardware 

These are Open Educational Resources (OER)
used under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

[source](https://gitlab.com/osh-academy/oer-osh-docu-1/-/blob/master/slides/s-docu-osh.md)

>[20.04.2021 - Timm Wille](https://wiki.opensourceecology.de/En:Timm_Wille)

<!--- 45min in total -->

---

## Recap OSH Basics

---

## What is Open Source Hardware?

<span>"[…] hardware whose design is made publicly available so that anyone can 
    study, modify, distribute, 
    make, and sell 
    the design or hardware based on that design."<!-- .element: class="fragment" data-fragment-index="1" --></span> 
<span>([OSHWA](https://www.oshwa.org/definition))<!-- .element: class="fragment" data-fragment-index="1" --></span> 


Note:
    - That's the definition from the Open Source Hardware Association.
    - So no dependencies.
    - You are publishing design files and just anyone could build and sell the hardware.

---

## Why does Openness matter?

Open Source Hardware can…

- <span>speed up innovation<!-- .element: class="fragment" data-fragment-index="1" --></span> 
- <span>get extra skills on board<!-- .element: class="fragment" data-fragment-index="2" --></span> 
- <span>what else?<!-- .element: class="fragment" data-fragment-index="3" --></span> 

Note: 
    - repeat from intro slides (headhunting etc.)
    - more ...

---

### Excample Circular Economy

<iframe width="1024" height="576" data-src="https://www.youtube.com/embed/dJ8DIn2vEV0?start=48" allowfullscreen data-autoplay></iframe>


---

## Short Brainstorm

## What is the purpose of documentation?

5 min

Note:
    - grab your neighbour and discuss
    - write downs the essencials 
    - why, what, for who, collaboration, ip
    - examples of documentation of proprietary hardware products -> what do they contain?

---

### What is the difference between open and closed technical documentation?

theoretically **none** except the accessibility and degrees of freedom and:
- feedback and request management of documentation content. 
- the 'user' can directly copy/edit the documentation for own purposes
- what else?

---

## ++Open Source++ Hardware brings new challenges

- Openness → licenses
- Platform → community discussion
- Source → accessibility to files and information
- Guidance → stakeholder collaboration and contribution
- Release → constant evolution and version controle

---

as well as 
- Readme → what is it all about?
- Forum → issue management and communication
- Read Only → some source information will need special output formats 
- Maintenance → Response, Quality, Improvements 
>Nothing happens on its own, keep it alive!

Note:
    - Branches, Forks, Modularity, Scalability, constant Improvements
    - Business Case, Service, Moderation, challenges

---

## Degrees of Documentation 
>**Know towards WHO you write the documentation for**

---

**Example 1:**
 - Tutorials (learning)
 - HowTo (problem-solving)
 - Explanation (understanding)
 - Reference (information)

[source](https://diataxis.fr/)

---

But: 
- where does the assembly guide fit?

---

**Example 2:**
- contribution guide </br>
    - getting to know tools, infrastructure, community, ...
- how-to guide </br>
    - define setup, interfaces, integration in defferent modules

</br></br>

and...

Note: 
- (guide people to contribute to your hardware project)
- (know how to install and maintain the hardware)

---

- user tutorials </br>
    - describe user interfaces, safety instructions, limits of usage, ...
- developer guide </br>
    - technical foundations, used methods, design process

</br></br>

and...

Note:
- (guidance around how to use the hardware)
- (guidance to develop the hardware)

---


- production/assembly guideline (guidance to rebuild hardware) </br>
    - BOMs, needed materials,  tools, production process, ...
</br></br>

→ **this might be the main difference btw software & hardware**

---

## ...and many more combinations of the above
yet to be defined by "you"

(you might want to check out "DIN SPEC 3105" standard)

Note:
    - Infrastructure
    - Logistics
    - etc.

---

### further reference
- https://opensource.com/business/13/6/four-types-organizational-structures-within-open-source-communities
- https://wackowiki.org/doc/Org/Articles/5TypesOpenSourceProjects
- https://opensource.com/article/17/5/five-steps-documentation
- https://opensource.guide/how-to-contribute/
- https://opensource.guide/best-practices/
- https://diataxis.fr/


---

<!-- .slide: data-transition="zoom" -->

# Exercise 

---

## Now it's time to get hands on experience

---

## Build a simple Match Rocket



... so others can build upon what you have started

<img src="https://gitlab.com/osh-academy/oer-osh-docu-1/-/raw/master/images/rocket.jpg" style="border: none;background: none;box-shadow:none" height="400">


---

<!-- .slide: data-background="https://github.com/opencultureagency/Solar.mini/raw/master/OpenHardwareGuide_5_A3-solar.mini.png" data-background-color="#000" -->

![Circuit diagram](https://gitlab.com/osh-academy/osh-basics/-/raw/master/graphics/documentation-of-osh/circuit2.png)

Note:
    - that you just need to replace "blob" by "raw" to display pictures from git-repositories

---

## Tools & Materials

* Aluminium foil
* Lighter/Torch
* Matches
* Tape
* Wood sticks/skewer
* Scissors

---

Choose a <span><!-- .element: class="fragment highlight-red" data-fragment-index="5" -->license</span> <span>← super important<!-- .element: class="fragment" data-fragment-index="5" --></span> 


---

# FAQ

---

## Group action 1 - Documentation

→ [Group-action](https://) (to add)

- Group forming
- Get your Documentation and Build Setup started
- you can use https://md.opensourceecology.de to create a collaborative Markdown documentation
- who is responsible for what? pictures?
- start prototyping


---

## Group action 2 - Collaboration

- Continue others work the way you would be able to replicate and present it
- Testing, final check
- "Respawn" to your original and be ready to present the collaboration

---

# Closing Pitch and Feedback


