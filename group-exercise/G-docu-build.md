[Group] Build & document action
===

connected to: [s-docu-osh.md](https://gitlab.com/osh-academy/osh-basics/-/blob/master/slides/s-docu-osh.md)

## Meta

- 2½h
- 5 members per team
  - 3 physical present
  - 2 online
- split among x different rooms

## Plan

### Intro & Preparation [30min]

- introduction (tools, materials, online-collaboration) + FAQ [15]
- form groups + preparation [15]

### 1st round | build the thing [1h]

- setup documentation and space
- start building
- document on platform of choice (e.g. [HedgeDoc](https://md.opensourceecology.de))

### Interruption

- trainer announces the "unexpected" end of the prototyping

### 2nd round | swap [1h]

- continue a different group's work
- condense learnings
- go back to your original documentation/project and present → Pitch final outcome
