# OER OSH-Docu-1

## Documentation of OSH

- what is the key difference between open and closed documentation?
- why is open source documentation not a single checklist?
- what are the main elements (to start with) of Open Source Projects?
- Example ~~Solar Charger~~ Matchbox Rocket 
- what happens when you build your own hardware and look at it from the documentations side?
- from documentation to collaboration
- platforms, tools, improvements


### Plan:

- Intro [15min]
- Why documentation is key and a never ending story [25min]
    - [slide show](https://md.opensourceecology.de/p/QSMWHjITF) ([source](https://gitlab.com/osh-academy/oer-osh-docu-1/-/blob/master/slides/s-docu-osh.md))
- Exercise part 1 - How to build a simple ~~Solar Charger~~ Matchbox Rocket [90min]
    - → introduction (tools, materials, online-collaboration) + FAQ [15min]
    - → form groups + preparation [15min]
    - → prototyping + documentation [60min]
- Exercise part 2 - Decentralised collaboration [60min]
    - → new setup - new challenge
    - → how to improve collaboration?
    - → what are the learnings and what is next?
- Pitch your results
- Feedback and Checkout [15min]
